## Abstract Factory


### What is the objective?

Allows the creation of an instance of a class from asuite of related classes without having a client object to specify the actual concrete class to be instantiated.

### What is an abstract factory?

Is a class that provides an interface to produce a family of objects.

#### How can it be implemented in Java?

An abstract factory can be implemented either as an interface or as an abstract class.

#### When is it used?

To created groups of reelated objects while hiding the actual concrete classes.

### When is it useful?

When a client object wants to create an instance of one of a suite of related, dependent classes without having to know which specific concrete class is to be instantiated.

Furthermore, for plugging in a diferent group of objects to alter the behavior of the system.

### What is the required implementation in absence of an abstract factory?

The class selection criterion needs to be present everywhere such an instance is created.

### Does an abstract factory help?

Yes, to avoid this duplication by providing the necessary interface for creating such interfaces.



### Reference

Kuchana, 2004, pages 92 to 94.
