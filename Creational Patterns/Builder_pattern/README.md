## Builder


### When is it useful?

To create a complex object


### What information does it give?

The type and content about the object.


### Advantages

- Allows the same construction process to produce different representations of the object.

- Keeps the details of the object creation process transparent to the client.


### Reference

Kuchana, 2004, page
