## Prototype

"A system should be independent of the creation, composition and representation details of the objects it uses"

### What is the objective?

Simple way of creating an object by cloning it from an existing (prototype) object.

### Other uses...

- When a client needs to create a set of objects (that differ from each other only in state), and it is expensive (time and processing) to create them.

- As an alternative to build factories (as Factory Method)

### What does this pattern suggest?

- Design one object as a prototype object

- Create other objects, making a copy of the prototype object with the modifications.

### It is used in the real world, isn't it?

We use it to reduce time and effort. Example: Use existing code and reuse it.

### What is the way for a client to create a copy?

There are two ways to copy an object using prototype pattern:

- Shallow copy:

Any lower-level objects that the original top-level object contains are not duplicated.

Only references to these objects are copied.

Both, the original and the cloned object referring to the same copy.

- Deep copy:

Any lower-level objects that the original top-level object contains are also duplicated.

Both, the original and the cloned object refer to two different lower-level objects.



### Reference

Kuchana, 2004, pages 108 to 110
