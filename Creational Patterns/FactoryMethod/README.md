## Factory Method


### What is the Objective?

Decide the appropiate class from a class hierarchy, which of them and how to instantiate it.


### Which are the factors to decide it?

- The state of the running application

- Application configuration settings

- Expansion of requirements or enhancements


### When is it useful?

When a client object does not know which class to instantiate.

It is used when a client object knows when to create an instance of the parent class type.


### Which does this pattern recommend?

Encapsulate the functionality required, inside a designed method referred to as a FACTORY METHOD.

A factory method:

- Selects an appropiate class from a class hierarchy (based on factors)

- Instantiates the selected class and returns it as an instance of the parent class type

#### The simplest way to design a factory method

Create an abstract class or an interface that just declares it.


### Advantages

- Delete the need for an application object to deal with the varying class selection criteria

- Delete the need to instantiate the appropiate class, it hides these details.


### Disadvantages

- High degree of coupling between an application object and the service provider class hierarchy.

- If the class selection criteria change, every application object that uses the class hierarchy must undeergo a corresponding change.

- The implementation of an application object could contain inelegant conditional statements.


#### Reference:

Kuchana, 2004, pages 78 to 80.




