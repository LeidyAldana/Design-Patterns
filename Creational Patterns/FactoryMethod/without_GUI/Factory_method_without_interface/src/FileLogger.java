/*
*   This code has been published by Partha Kuchana, 
*   in his book Software architecture design patterns in Java
*   ISBN 0-8493-2142-5
*/

public class FileLogger implements Logger {

  public void log(String msg) {
    FileUtil futil = new FileUtil();
    futil.writeToFile("log.txt",msg, true, true);
  }

}