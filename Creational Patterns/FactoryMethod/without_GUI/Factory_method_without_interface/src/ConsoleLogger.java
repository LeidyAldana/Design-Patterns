/*
*   This code has been published by Partha Kuchana, 
*   in his book Software architecture design patterns in Java
*   ISBN 0-8493-2142-5
*/

public class ConsoleLogger implements Logger {

  public void log(String msg) {
    System.out.println(msg);
  }

}