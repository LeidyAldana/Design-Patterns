/*
*   This code has been published by Partha Kuchana, 
*   in his book Software architecture design patterns in Java,
*   Chapter 11, Singleton, Listing 11.1
*   ISBN 0-8493-2142-5
*
*/
public class FileLogger implements Logger {

  private static FileLogger logger;

  //Prevent clients from using the constructor
  private FileLogger() {
  }

  public static FileLogger getFileLogger() {
    if (logger == null) {
      logger = new FileLogger();
    }
    return logger;
  }

  public synchronized void log(String msg) {
    FileUtil futil = new FileUtil();
    futil.writeToFile("log.txt",msg, true, true);
  }

}
