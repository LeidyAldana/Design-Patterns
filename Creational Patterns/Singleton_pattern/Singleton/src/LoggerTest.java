/*
*   This code has been published by Partha Kuchana, 
*   in his book Software architecture design patterns in Java,
*   Chapter 10, Factory Method
*   ISBN 0-8493-2142-5
*
*/

public class LoggerTest {

  public static void main(String[] args) {
    LoggerFactory factory = new LoggerFactory();
    Logger logger = factory.getLogger();
    logger.log("A Message to Log");
  }

}
