## Singleton


### What is the objective?

Provide a controlled object creation mechanism to ensure that only one instance of a given class exist.


### When is it useful?

To ensure that exists one and only one instance of a particular object.


### Who should be responsible?

All of the client objects have to. However, a client should be free from any class creation process details.

So, the responsability for making sure that there is only one instance of the class should belong to the class itself.


### What is a Singleton class?

A class that maintains its single instance nature by itself.


### What is the monitor concept?

It ensures that no two threads are allowed to access the same object at the same time.


### Suggestions

- Make the constructor private
- Static Public Interface to Access an Instance


### Reference

Kuchana, 2004, pages 87 to 91.
