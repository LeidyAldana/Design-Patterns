## Creational Patterns


### What are the objectives:

- Encourage the use of interfaces, which reduces coupling.

- Support a uniform, simple and controlled mechanism to create objects.

- Allow the encapsulation of the details about what classes are instantiated and how these instances are created.

- Deal with the creation of objects.


### Which patterns will we find?

- Factory Method

- Singleton

- Abstract Factory

- Prototype

- Builder


### Reference:

Kuchana, 2004, page 78.

