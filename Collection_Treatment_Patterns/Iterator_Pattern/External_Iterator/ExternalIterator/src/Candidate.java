public class Candidate {
  private String name;
  private String certificationType;
  private String location;
  private String profesion;

  public Candidate(String inp_name, String inp_certType,
                   String loc, String profes) {
    name = inp_name;
    certificationType = inp_certType;
    location = loc;
    profesion = profes;
  }

  public String getCertificationType() {
    return certificationType;
  }
  public String getName() {
    return name;
  }
  public String getLocation() {
    return location;
  }
   public String getProfesion() {
    return profesion;
  }
}
