## Counting Proxy

### When is it useful?

In designing a set of additional operations that need to be performed before and/or after a client object invokes a method on a service provider object.

### What does suggest this pattern?

Encapsulate the additional functionality in a separated object (CountingProxy)

### Why is a counting proxy designed?

To have the same interface as the service provider object that the client access.



Counting Proxy --> Útil al tener un conjunto de operaciones adicionales.
Esa funcionalidad adicional es encapsulada en un objeto separado (CountingProxy),
el cual tiene la misma interfaz que el objeto proveedor del serivcio al que el cliente accesa.
