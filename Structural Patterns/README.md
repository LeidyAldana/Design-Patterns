## Structural patterns

### Functionality

- Deal with objects delegating responsabilities to other objects (layered architecture of components with low degree of coupling)

- Facilitate interobject communication when:
	one object is not accesible to the other (by normal means)
	an object is not usable cause of its iconmpatible interface

- Provide ways to structure an aggregate object so that it is create in full and to reclaim system resources in a timely manner


### Which patterns will we find?

- Decorator

- Adapter

- Chain of Responsability

- Facade

- Proxy

- Bridge

- Virtual Proxy

- Counting Proxy

- Aggregate Enforcer

- Explicit Object Release

- Object Cache
