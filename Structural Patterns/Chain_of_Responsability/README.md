## Chain of Responsability (CoR)

It recommends a low degree of coupling between an object that sends out a request and the set of potential request handler objects.

### What does recommend this pattern if there is more than one object that can handle a request?

When there is more than one object that can handle or fulfill a client request, the pattern recommends giving each of these objects a chance to process the request in some sequential order.

### Can these objects be arranged?

Each of these potental handler objects can be arranged in the form of a chain, with each object having a pointer to the next object in the chain.

### How does this chain work?

The first object in the chain receives the request and decides either to handle the request ot to pass it on to the next object in the chain.

### Can the chain guarantee that the request will get processed?

"A request submitted to the chain of handlers may not be fulfilled even after reaching the end of the chain"

The request flows through all objects in the chain one after the other until the request is handled by one of the handlers in the chain or the request reaches the end of the chain without getting processed.

### Example:

A --> B --> C	{A,B,C are objects capable of handling the request}

- A should handle the request or pass on to B withput determining whether B can fulfill the request.

- Upon receiving the request, B should either handle it or pass on to C.

- When C receives the request, it should either handle the request or the request falls of the chain without getting processed.

### Characteristics CoR pattern

#### - When can be decided the chain? 

The set of potential request handler objects and the order in which these objects form the chain can be decided DYNAMICALLY AT RUNTIME by the client depending on the current state of the application.

### - Can exist different types of request?

A client can have different sets of handler objects for different types of requests depending on its current state.

A given handler object may need to pass on an incoming request to different other handler objects depending on the request type and the state of application.

For these communications to be simple, all potential handler objects should provide a consistent interface.

In Java, this can be accomplished by having different handlers implement a common interface or be subclasses of a common abstract parent class.

### - Who does not have to know the capabilities of the object receiving the request?

The client object that initiates the request or any of the potential handler objects that forward the request.

"Neither the client object nor any of the handler objects in the chain need to know which object will actually fulfill the request."

### - Is request handling guaranteed?

No, the request may reach the end of the chain without being fullfilled.


## Source:

Kuchana, 2014, pages 227 to 228




