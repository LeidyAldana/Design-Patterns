## Concurrency patterns

### They deal with...

- Ways to lock class code and an order of locking objects to prevent the ocurrence of race conditions and deadlocks

- Details to simplify access to an application resource, in order to improve the capacity of answer

- Details of method execution while a required precondition is not met

### Which patterns will we find?

- Critical section

- Consistent Lock Order

- Guarded Suspension

- Read-Write Lock

