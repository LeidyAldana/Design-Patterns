
package CriticalSection;

public class FileThread extends Thread{
    private String msg;

    public FileThread(String msg) {
        this.msg = msg;
        this.start();
        this.run();
    }
    
    public void run(){
        FileLogger fileLogger=FileLogger.getFileLogger();        
        fileLogger.log(msg);
    }
    
}
