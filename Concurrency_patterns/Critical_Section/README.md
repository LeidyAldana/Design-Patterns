## Critical Section

### What is a critical section?

It is a segment of code that must be executed by only one thread at a time to produce the expected results.

#### What happens if more than one there were allowed to execute this code segment?

It could produce unpredictable results.


