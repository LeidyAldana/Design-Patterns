
public class FileThread extends Thread{
    private Directory source;
    private Directory resource;

    public FileThread(Directory source, Directory resource) {
        this.source=source;
        this.resource=resource;
        this.start();
        this.run();
    }
    
    public void run(){
        FileSysUtil_Rev fur = new FileSysUtil_Rev();
        fur.moveContents(source, resource);
    }

}
